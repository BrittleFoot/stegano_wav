import unittest


class wav_hideing(unittest.TestCase):

    def test_hide_smth_like_wav(self):
        from stegwav.custom_read import WaveFrameReader
        from stegwav.wav import WaveReader, WaveWriter
        from stegwav.assets import get_asset
        from stegwav.reading import apply_reading_bits
        from stegwav.hiding import hide
        from stegwav.string_ext import to_bytes_from_bin

        music = get_asset('Tolstoy_WOLK.wav')

        mr = WaveReader(music)
        mbr = WaveFrameReader(mr)

        data_name = get_asset('anarchist.wav')
        data = open(data_name, mode='rb')
        data = apply_reading_bits(data)

        # mask - one bit in one channal
        mask = '0000000000000001' * 2

        hiden = hide(mbr, mask, data)

        def translate_to_bytes(bin_generator):
            return map(to_bytes_from_bin, bin_generator)

        output = WaveWriter(data_name, mr.properties)

        byte_gen = translate_to_bytes(hiden)
        output.write_from_byte_generator(byte_gen, 1)

        mr.close()

        #_____________________________

        from stegwav.extracting import extract
        from stegwav.string_ext import collect_to_pockets, extract_to_bytes

        wr = WaveReader(output.name)
        filled = WaveFrameReader(wr)
        extracted = extract(filled, mask)

        f = open(get_asset('decoded.wav'), mode='wb')

        size = wr.properties[-1] / (filled._iter_count * 32) * mask.count('1')
        counter = 0
        curr_size = 0

        for bts in collect_to_pockets(extract_to_bytes(extracted), 1024):
            f.write(bts)
            counter += len(bts)
            curr_size += len(bts)

            if counter > 10000:
                counter = 0
                prcnt = round(curr_size * 100 / size, 2)
                print('\r%s\rDecodeing: %s %%' % (' ' * 40, prcnt), end='')
        print()
        print('Done!')
        f.close()


if __name__ == '__main__':
    unittest.main()
