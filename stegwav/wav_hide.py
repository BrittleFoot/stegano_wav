from stegwav.custom_read import WaveFrameReader
from stegwav.reading import FileBitGenerator
from stegwav.wav import WaveReader as read, WaveWriter as write
from stegwav.hiding import hide as hide_data
from stegwav.string_ext import translate_to_bytes


class Error(Exception):
    pass


def hide(args):
    mask = args.mask

    if len(mask.replace('0', '').replace('1', '')) > 0:
        raise Error('Invalid mask: mask is string from 0 and 1')

    wr = read(args.src_file)

    if args.channels is None:
        args.channels = '1' * wr.NumChannels
    elif len(args.channels.replace('0', '').replace('1', '')) > 0:
        raise Error('Invalid channels: channels is string from 0 and 1')
    elif len(args.channels) > wr.NumChannels:
        raise Error('Invalid channels: %s is max mask for this .wav' %
                    ('1' * wr.NumChannels))
    elif len(args.channels) < wr.NumChannels:
        err = 'Invalid channels: length of this mask should be %s'
        raise Error(err % wr.NumChannels)

    if len(mask) > wr.ChannelSize * 8:
        eargs = wr.ChannelSize * 8, len(mask)
        raise Error('Invalid mask length: expected <=%s, got %s' % eargs)

    # alighn mask by channel mask and channel size
    mask = (('%%%ss' % (wr.ChannelSize * 8)) % mask).replace(' ', '0')
    chan = args.channels.replace('0', '0' * wr.ChannelSize * 8)
    chan = chan.replace('1', mask)
    mask = chan

    source = WaveFrameReader(wr)
    data = FileBitGenerator(open(args.data_file, mode='rb'))

    hiden = translate_to_bytes(hide_data(source, mask, data))

    write(args.out_file, wr.properties).write_from_byte_generator(hiden, 1)
    wr.close()
