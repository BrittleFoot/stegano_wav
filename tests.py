"""
    agregate of stegwav module tests
"""
import unittest
import logging as log
print


class reader_tests(unittest.TestCase):

    """ functionality of reading module """

    def assertReaderWorks(self, expected, reader):
        from stegwav.string_ext import to_bytes_from_bin
        print('Test of %s on %s...!' %
              (reader.__class__.__name__, expected[:42]))
        reader.set_iter_count(3)
        res = ''

        for bits in reader:
            res += bits

        self.assertEqual(len(expected) * 8, len(res))

        res = to_bytes_from_bin(res)

        self.assertEqual(expected, res)
        print('Succes!')

    def test_read_bits_from_bytes(self):
        from stegwav.reading import apply_reading_bits

        bts = b'123' * 100
        reader = apply_reading_bits(bts)

        self.assertReaderWorks(bts, reader)

    def test_read_bits_from_file(self):
        from stegwav.reading import apply_reading_bits
        from stegwav.assets import get_asset

        txt_file_name = get_asset('test_secret_message.txt')
        with open(txt_file_name, mode='rb') as file_inst:
            expected = file_inst.read()

        secret_file = open(get_asset('test_secret_message.txt'), mode='rb')

        reader = apply_reading_bits(secret_file, 8)

        self.assertReaderWorks(expected, reader)

    def test_read_bytes_from_bytes(self):
        print('ReadBytesTest begins:')
        from stegwav.reading import BytesByteGenerator
        from stegwav.string_ext import to_bytes_from_bin

        example = b'\x01\x03\x07\x0f'
        ebbg = BytesByteGenerator(example, 10)
        returned = ebbg.readbytes(4)

        res = to_bytes_from_bin(returned)

        self.assertEqual(example, res)
        print('Succes!')


class hide_tests(unittest.TestCase):

    def test_bytes_seq_encode_and_decode(self):
        from stegwav.reading import BytesByteGenerator
        from stegwav.hiding import hide

        from stegwav.reading import apply_reading_bits
        from stegwav.assets import get_asset
        from stegwav.string_ext import to_bytes_from_bin

        txt_file_name = get_asset('test_secret_message.txt')
        with open(txt_file_name, mode='rb') as file_inst:
            expected = file_inst.read()

        secret_file = open(get_asset('test_secret_message.txt'), mode='rb')
        bit_reader = apply_reading_bits(secret_file)

        string_to_be_changed = b'kw' * 100
        byte_reader = BytesByteGenerator(string_to_be_changed, 2)

        mask = '101'

        res = ''.join(hide(byte_reader, mask, bit_reader))
        res = to_bytes_from_bin(res)

        print('Succesfuly masked "%s"!' % expected)
        print('Before:', string_to_be_changed[:20] + b'...',
              '\nAfter :', res[:20] + b'...')

        from stegwav.extracting import extract

        enc_reader = BytesByteGenerator(res, 2)

        extracted = extract(enc_reader, mask)

        origin = ''.join(extracted)
        origin = to_bytes_from_bin(origin)

        self.assertTrue(origin.startswith(expected))
        print('Succesfuly decoded, resultat is: %s' % origin)


class wav_tests(unittest.TestCase):

    def test_reading_parseing_writing(self):
        from stegwav.assets import get_asset
        from stegwav.wav import WaveReader
        import wave

        music = get_asset('Tolstoy_WOLK.wav')

        mywav = WaveReader(music)
        props = mywav.properties
        print(props)

        fms = mywav.read_frames(4000)

        mywav.close()

        wav = wave.open(music, mode='rb')
        fms1 = wav.readframes(4000)

        self.assertEqual(fms, fms1)
        wav.close()

        print('Wow! Your wave reader works the same as the built in!')

        from stegwav.wav import WaveWriter

        mywav = WaveReader(music)
        out = WaveWriter(music, mywav.properties)

        def iterate(mywav):
            while 1:
                bts = mywav.read_frames(1024 * 1024)
                if not bts:
                    break
                yield bts

        out.write_from_byte_generator(iterate(mywav))


if __name__ == '__main__':
    unittest.main()
