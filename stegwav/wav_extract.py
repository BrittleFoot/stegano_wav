from stegwav.custom_read import WaveFrameReader
from stegwav.reading import FileBitGenerator
from stegwav.wav import WaveReader as read
from stegwav.extracting import extract as extract_data
from stegwav.string_ext import translate_to_bytes
from stegwav.string_ext import extract_to_bytes
from stegwav.string_ext import collect_to_pockets


class Error(Exception):
    pass


def extract(args):
    mask = args.mask

    if len(mask.replace('0', '').replace('1', '')) > 0:
        raise Error('Invalid mask: mask is string from 0 and 1')

    wr = read(args.src_file)

    if args.channels is None:
        args.channels = '1' * wr.NumChannels
    elif len(args.channels.replace('0', '').replace('1', '')) > 0:
        raise Error('Invalid channels: channels is string from 0 and 1')
    elif len(args.channels) > wr.NumChannels:
        raise Error('Invalid channels: %s is max mask for this .wav' %
                    ('1' * wr.NumChannels))
    elif len(args.channels) < wr.NumChannels:
        err = 'Invalid channels: length of this mask should be %s'
        raise Error(err % wr.NumChannels)

    if len(mask) > wr.ChannelSize * 8:
        eargs = wr.ChannelSize * 8, len(mask)
        raise Error('Invalid mask length: expected <=%s, got %s' % eargs)

    # alighn mask by channel mask and channel size
    mask = (('%%%ss' % (wr.ChannelSize * 8)) % mask).replace(' ', '0')
    chan = args.channels.replace('0', '0' * wr.ChannelSize * 8)
    chan = chan.replace('1', mask)
    mask = chan

    source = WaveFrameReader(wr)
    extracted = extract_data(source, mask)
    extracted = collect_to_pockets(extract_to_bytes(extracted), 1024)
    out_file = open(args.out_file, mode='wb')

    size = wr.FramesCount / 8 * mask.count('1')
    counter = 0
    pointer = 0

    for bts in extracted:
        out_file.write(bts)

        counter += len(bts)
        pointer += len(bts)

        if pointer > 10000:
            pointer = 0
            prcnt = round(counter * 100 / size, 2)    
            print('\r%s\rDecodeing: %s %%' % (' ' * 40, prcnt), end='')
    print()
    print('Done, check %s!' % args.out_file)

    out_file.close()
    wr.close()



