"""
Custom readers for dofferent hideing ways.
"""

from stegwav.reading import IByteGenerator, byte_to_fixed_bin


class WaveFrameReader(IByteGenerator):

    """
        Basic wave reader, applyes pre frame hideing
        (frame - following one by one descriptors of channel tick)
    """

    def __init__(self, wave_reader):
        self._wr = wave_reader
        self.itersize = self._wr.FrameSize
        self._iter_count = 1

    def __del__(self):
        self._wr.close()

    def get_iter_count(self):
        return self._iter_count * self.itersize

    def readbytes(self, n):
        return ''.join(map(byte_to_fixed_bin, self._wr.read_frames(n)))
