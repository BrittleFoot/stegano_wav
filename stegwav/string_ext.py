"""
    extends methods to work with strings
"""


def chunkstring(string, length):
    """
        generate splitted into fixed length chunks string 
    """
    return (string[0+i:length+i] for i in range(0, len(string), length))


def to_bytes_from_bin(bin_string):
    """
        returns bytes parsed from string

        '00000001000000110000011100001111' -> b'\x01\x03\x07\x0f'
    """
    return bytes(map(lambda x: int(x, 2), chunkstring(bin_string, 8)))


def translate_to_bytes(bin_generator):
    """
        it is funcions 'to_bytes_from_bin' for generators
    """
    return map(to_bytes_from_bin, bin_generator)


def extract_to_bytes(extracted):
    """
        collects data from bin generator
        and converitng it to bytes
    """
    buffer = ''

    for x in extracted:
        buffer += x
        if len(buffer) >= 8:
            yield int(buffer[:8], 2)
            buffer = buffer[8:]

    if buffer:
        yield int(buffer, 2)


def collect_to_pockets(byts, size):
    """
        sollect data from generator, wich send 1 byte per next()
        to fixed size pockets 
    """
    buffer = []

    for x in byts:
        buffer.append(x)
        if len(buffer) >= size:
            yield bytes(buffer)
            buffer = []

    if buffer:
        yield bytes(buffer)
