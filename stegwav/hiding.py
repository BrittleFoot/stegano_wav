"""
# Модуль с функциями сокрытия информации 'a' в информации 'b'

# Вход:
#     - обьект выдающий по N байт (almost done. see readig)
#     - битовая маска подмены информации в этих байтах
#     - обьект, выдающиий необходимое маске колличество бит (done. see readig)
# Выход:
#     - генератор отрезков длины N с зашифроваными в них кусками информации
"""

__all__ = ['hide', 'hide_in']


def hide_in(bts_sequence, mask, data_part):
    """
        single step of global function hide
        applyed data_part hiding in concrete byte sequence
        For independed use check that len(source) == len(mask)
        and len(data_part) == mask.count('1')
    """

    if not data_part:
        return bts_sequence, True

    res = []
    data_counter = 0
    for i in range(len(bts_sequence)):
        if mask[-1 - i] == '1' and data_counter < len(data_part):
            data_counter += 1
            res.append(data_part[-data_counter])

        else:
            res.append(bts_sequence[-1 - i])

    return ''.join(res[::-1]), False


def hide(source, mask, data):
    """
        source - IByteReader from stegwav.reading
        mask - rule by changing bits with data
        data - IBitReader from stegwav.reading

        1) confifurateing paremeters:
            note that data reader will be configurated in current function
                                    (especialy with mask)
            but source reader should be costumized in advance
                                    (сount of issueing bytes in for cycle)

        !!!
            check that mask length is not greater that bytes_per_for_cycle * 8
            because we do not know how to hide '11111' in '1111' (as example)

        2) Mask:
            as example look at mask '100101' it iddeclare two things
                1) volume:
                    three chars "1" says that in one bytes sequince
                    three bits of data will be hiden
                2) order:
                    0, 2 and 4th bits
                    will be used for hide data in single byte sequence
                                                      v  v v
                    if there are two bytes '1111111111111111' then marked by (v)
                    bits will be used for hide data

                    and inwe get three bits of data like '000'
                    then function returns  '1111111111011010'
                    according mask                   '100101'
    """
    seq_length = source.get_iter_count() * 8
    if len(mask) > seq_length:
        err = 'expectes len(mask) <= %s' % seq_length
        raise ValueError('mask too large for this source reader, %s' % err)

    mask_volume = mask.count('1')
    mask = (('%%%ss' % seq_length) % mask).replace(' ', '0')

    for sequence in source:
        hiden, bits_empty =  hide_in(sequence, mask, data.readbits(mask_volume))
        if bits_empty:
            source.set_iter_count(1024)
        yield hiden
