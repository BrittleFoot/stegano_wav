"""
#   Module provide functions and tools to extract info from
#   bytes of encoded data
"""

__all__ = ['extract', 'extract_in']


def extract_in(source, mask):
    """
        Single step of extract function.
        For independed use check that len(source) == len(mask)
    """
    res = []
    for i in range(len(source)):
        if mask[i] == '1':
            res.append(source[i])

    return ''.join(res)


def extract(source, mask):
    """Extract bits from source by mask.

    Args
        source - IByteReader object, already configurated
            for iterateing in right sequence
        mask - bit mask for extract bytes in byte sequence

    (v) opposite function of stegwav.hiding.hide
    """

    seq_length = source.get_iter_count() * 8
    if len(mask) > seq_length:
        err = 'expectes len(mask) <= %s' % seq_length
        raise ValueError('mask too large for this source reader, %s' % err)

    mask = (('%%%ss' % seq_length) % mask).replace(' ', '0')

    for sequence in source:
        yield extract_in(sequence, mask)
