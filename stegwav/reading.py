"""
    Tools for reading bits and bytes form files and bytes c:
"""
import wave
import _io

__bin = bin
bin = lambda n: (__bin(n)[2:]).replace('-', '')
byte_to_fixed_bin = lambda b: ('%8s' % bin(b)).replace(' ', '0')


"""

    BITS PART

"""


class IBitGenerator:

    """
    object, witch can give you requried amount of target
    bits in big endian order
    """

    def __init__(self, iter_count):
        """
            iter_count - amount of bytes taken inf foreach cycle
        """
        self._iter_count = iter_count

    def set_iter_count(self, iter_count):
        self._iter_count = iter_count

    def readbits(self, n):
        """

        return n bit of something

        """
        raise NotImplementedError()

    def __iter__(self):
        return self

    def __next__(self):
        data = self.readbits(self._iter_count)
        if not data:
            raise StopIteration()
        return data


class BytesBitGenerator(IBitGenerator):

    """BitGenerator for instances of bytes (primary with light weights)"""

    def __init__(self, bts, iter_count=1):
        super().__init__(iter_count)

        self._base = ''.join(map(byte_to_fixed_bin, bts))
        self._base_index = 0

    def readbits(self, n):
        res = self._base[self._base_index: self._base_index + n]
        self._base_index += n
        return res


class FileBitGenerator(IBitGenerator):

    """BitGenerator for files in rb mode"""

    def __init__(self, rbfile, iter_count=1):
        super().__init__(iter_count)
        self.rbfile = rbfile

        self._buffer = ''

    def __del__(self):
        self.rbfile.close()
        self.rbfile = None

    def readbits(self, n):

        if len(self._buffer) < n:
            new_data = self.rbfile.read(n // 8 + 1)
            self._buffer += ''.join(map(byte_to_fixed_bin, new_data))
        res = self._buffer[0: n]
        self._buffer = self._buffer[n:]

        return res

    def __next__(self):
        try:
            return super().__next__()
        except StopIteration as e:
            self.rbfile.close()
            raise e


def apply_reading_bits(obj, n_bits=1):
    """ automaticly choose IBitGenerator for obj """
    if isinstance(obj, bytes):
        return BytesBitGenerator(obj, n_bits)
    elif isinstance(obj, _io.BufferedReader):
        return FileBitGenerator(obj, n_bits)

    raise NotImplementedError()

"""

    BYTES PART

"""


class IByteGenerator:

    """
    object, witch can give you requried amount of target
    bytes in big endian order
    """

    def __init__(self, iter_count):
        """
            iter_count - amount of bytes taken inf foreach cycle
        """
        self._iter_count = iter_count

    def set_iter_count(self, iter_count):
        self._iter_count = iter_count

    def get_iter_count(self):
        return self._iter_count

    def readbytes(self, n):
        """

        return n bytes in bin format of something

        b'\x01\x03' -> '0000000100000111'

        """
        raise NotImplementedError()

    def __iter__(self):
        return self

    def __next__(self):
        data = self.readbytes(self._iter_count)
        if not data:
            raise StopIteration()
        return data


class BytesByteGenerator(IByteGenerator):

    """ByteGenerator for instances of bytes (primary with light weights)"""

    def __init__(self, bts, iter_count=1):
        super().__init__(iter_count)

        self._base = ''.join(map(byte_to_fixed_bin, bts))
        self._base_index = 0

    def readbytes(self, n):
        n = n * 8
        res = self._base[self._base_index: self._base_index + n]
        self._base_index += n
        return res
