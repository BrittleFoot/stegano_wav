"""
    Analys .wav file to be used in hide or extract modules
"""
from stegwav.wav import WaveReader, Error


def analys(args):
    """
        input: filename
        output: dictionary with info about .wav
    """
    wav = WaveReader(args.filename)

    return {
        'name': wav.name,
        'size': wav.ChunkSize + 4,
        'framesize': wav.FrameSize,
        'num_channels': wav.NumChannels,
        'channel_size': wav.ChannelSize,
        'frames_count': wav.FramesCount
    }


