"""
    Work with .wav filess
"""
from sys import exit
from struct import Struct
from os.path import basename

__all__ = ['WaveReader', 'WaveWriter', 'Error']


WAVE_HEADER_PACKER = Struct('<4si4s4sihhiihh4si')
WAVE_FORMAT_PCM = 16


class Error(Exception):
    pass


class WaveReader:

    """represents wave file with methods by reaing it"""

    def __init__(self, filename):
        self.name = basename(filename)
        self._wav = open(filename, mode='rb')
        self.__read_header()
        self.frame_pointer = 0

    def __read_header(self):
        self.raw_header = self._wav.read(WAVE_HEADER_PACKER.size)

        try:
            self.properties = WAVE_HEADER_PACKER.unpack(self.raw_header)
        except Exception:
            raise Error("Incorrect wav header")
        (
            self.ChunkID,
            self.ChunkSize,
            self.Format,
            self.Subchunk1ID,
            self.Subchunk1Size,
            self.AudioFormat,
            self.NumChannels,
            self.SampleRate,
            self.ByteRate,
            self.BlockAlign,
            self.BitsPerSample,
            self.Subchunk2ID,
            self.Subchunk2Size,
        ) = self.properties

        if self.ChunkID != b'RIFF':
            raise Error('Invalid for WAV ChunkID - %s' % self.ChunkID)
        if self.Format != b'WAVE':
            raise Error('Invalid for WAV Format - %s' % self.Format)
        if self.Subchunk1ID != b'fmt ':
            raise Error('Invalid for WAV Subchunk1ID - %s' % self.Subchunk1ID)
        if self.Subchunk1Size != WAVE_FORMAT_PCM:
            raise Error('Unsupported Format - %s' % self.Subchunk1Size)
        if self.Subchunk2ID != b'data':
            raise Error('Invalid for WAV Subchunk2ID - %s' % self.Subchunk2ID)

        self.FrameSize = self.BlockAlign
        self.FramesCount = self.Subchunk2Size // self.FrameSize
        self.ChannelSize = self.FrameSize // self.NumChannels

    def read_frames(self, count=1):
        if self.frame_pointer <= self.FramesCount:
            frames = self._wav.read(count * self.FrameSize)
            self.frame_pointer += 1
            return frames
        else:
            self.close()
            return b''

    def close(self):
        self._wav.close()
        return self

    def __del__(self):
        self._wav.close()


class WaveWriter:

    """class for write wav file"""

    def __init__(self, filename, properties):

        filename = filename.rsplit('.', maxsplit=1)[0] + '_ws.wav'
        while 1:
            try:
                self._file = open(filename, mode='wb')
            except OSError:
                filename = filename.rsplit('.', maxsplit=1)[0] + '_ws.wav'
            else:
                break
        self.name = filename

        self._file.write(WAVE_HEADER_PACKER.pack(*properties))
        self.properties = list(properties)

    def change_size(self, new_size):
        new_size = new_size
        HEADER_OFFSET = 36
        self.properties[-1] = new_size
        self.properties[+1] = new_size + HEADER_OFFSET

    def write_from_byte_generator(self, gen, show_timeline=False):
        new_size = 0

        old_size = self.properties[-1]

        FREQ = 100000
        cycler = 0
        for bts in gen:
            self._file.write(bts)
            new_size += len(bts)
            if show_timeline:
                cycler += len(bts)
                if cycler >= FREQ:
                    cycler = 0
                    prcnt = round(new_size / old_size, 4) * 100
                    print('\r%s\rHideing: %s %%' % (' ' * 40, prcnt), end='')
        if show_timeline:
            print('\nDone, check %s!' % self.name)
        self.change_size(new_size)
        self.close()

    def close(self):
        self._file.seek(0)
        self._file.write(WAVE_HEADER_PACKER.pack(*self.properties))
        self._file.close()
