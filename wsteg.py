"""
    Main proggramm to strganographing
"""

import argparse as ap


ANALYSE_HELP = """
for use hide or extract option:
    one frame consisits of linear following channels
    for this file we have:
        1 frame consists of %s channels of length %s
        so, maximal length of mask is (channel_size * 8) = %s
        you can mannualy choose wich channels will be used for you info.
"""


def analyse_it(args):
    from stegwav.wav_analys import analys
    import stegwav.wav

    try:
        analys_info = analys(args)
    except stegwav.wav.Error as e:
        print(e)
    else:
        for key, value in analys_info.items():
            print('%20s %-10s' % (key, value))

        add_info = ANALYSE_HELP % (analys_info['num_channels'],
                                   analys_info['channel_size'],
                                   analys_info['channel_size'] * 8
                                   )

        print(add_info)


def hide_it(args):
    from stegwav.wav_hide import hide

    try:
        hide(args)
    except Exception as e:
        print(e)
    except KeyboardInterrupt:
        print('\nKeyboard Interrupt!')
        print('Operation is not finished, but we have some results c:')


def extract_it(args):
    from stegwav.wav_extract import extract

    try:
        extract(args)
    except Exception as e:
        print(e)
    except KeyboardInterrupt:
        print('\nKeyboard Interrupt!')
        print('Operation is not finished, but we have some results c:')


def make_parser():

    # create the top-level parser
    parser = ap.ArgumentParser(prog='wsteg.py')
    subparsers = parser.add_subparsers(help='sub-command help')

    # create the parser for the "hide" command
    parser_hide = subparsers.add_parser('hide', help='hide data in src')
    parser_hide.add_argument('src_file')
    parser_hide.add_argument('data_file')
    parser_hide.add_argument('--out_file', '-o', default='steganographed.wav')
    parser_hide.add_argument('--mask', '-m', required=True)
    parser_hide.add_argument('--channels', '-c', help='mask for channels')
    parser_hide.set_defaults(func=hide_it)

    # create the parser for the "extract" command
    parser_extract = subparsers.add_parser('extract', help='extract help')
    parser_extract.add_argument('src_file')
    parser_extract.add_argument('--out_file', '-o', default='extracted.w_t_f')
    parser_extract.add_argument('--mask', '-m', required=True)
    parser_extract.add_argument('--channels', '-c', help='mask for channels')
    parser_extract.set_defaults(func=extract_it)

    # create the parser for the "analys" command
    parser_analys = subparsers.add_parser(
        'analys', help='analys given .wav file.')
    parser_analys.add_argument('filename', type=str)
    parser_analys.set_defaults(func=analyse_it)

    return parser


def main():
    parser = make_parser()
    args = parser.parse_args()
    if 'func' in dir(args):
        args.func(args)


if __name__ == '__main__':
    main()
