"""
    provide supporte to getting assets by name
"""

import os


def get_asset(name):
    return os.path.join(os.path.dirname(__file__), name)
