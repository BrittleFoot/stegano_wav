# Wave Steganography #

Current repo has scripts and tools for steganography
and .wav files steganography

## Requirements: ##
    python3

## Usage: ##
    main file is **wsteg.py**, follow it's own **help** c:

## Examples: ##

There is some examples of usage:

1) this command will create new file, the same, as Tolstoy_WOLK.wav, but with hided inthere anarchist.wav

        
```

python wsteg.py hide -c 01 -m 1 stegwav\assets\Tolstoy_WOLK.wav stegwav\assets\anarchist.wav
```

2) this command will extract info from steganographed_ws.wav, and create new file which (if you first execute command 1) will be file anarchist.wav
        
```
#!

python wsteg.py extract -m 1 -c 01 --out_file=extracted.wav steganographed_ws.wav
```